# Install Deno

Shell (Mac, Linux):

    curl -fsSL https://deno.land/x/install/install.sh | sh

PowerShell (Windows):

    iwr https://deno.land/x/install/install.ps1 -useb | iex

For mode details go to [deno.land](https://deno.land/)

# Generate fake input files

    deno run --allow-env --allow-read --allow-write https://bitbucket.org/jackfiszr/cocfinder/raw/master/create_mocks.ts --mock

Might also need '--unstable' flag.

# Split mutipage pdf docs into separate 2-page docs

For this to work, you have to have a command line utility 'pdftotext' installed.
If you are on a Linux machine, then it is probably included by default as part
of poppler-utils package. If you are on Windows, put the file from bin folder of
this repository into your system path.

    deno run --allow-env --allow-read --allow-write --allow-run https://bitbucket.org/jackfiszr/cocfinder/raw/master/split_cocs.ts --mock

Might also need '--unstable' flag.

# Search for and copy specific docs

Create file 'lista_vin.txt' with a list of vehicle identification numbers, e.g.:

    TMBZZZM0CKV1N0001
    TMBZZZM0CKV1N0002
    WVWZZZM0CKV1N0003
    WV1ZZZM0CKV1N0004
    WV2ZZZM0CKV1N0005
    WAUZZZM0CKV1N0006
    WUAZZZM0CKV1N0007
    TRUZZZM0CKV1N0008
    VSSZZZM0CKV1N0009
    VSSZZZM0CKV1N0010
    ...

Then run (from the same direrctory as the list file):

    deno run --allow-env --allow-read --allow-write https://bitbucket.org/jackfiszr/cocfinder/raw/master/copy_cocs.ts --mock

In the localization the script is executed from, a directory named with your
system username will be created, and all the found docs will be copied to it.

Use '--copy=last' script argument if you want to find only one copy of each doc
(the newest one).

Might also need '--unstable' flag.

# Index the split docs

Indexing the docs allows for a much faster search time in case of very large
file sets

(ensure you have 'index.txt' file in the split docs dir)

    deno run --allow-env --allow-read --allow-write https://bitbucket.org/jackfiszr/cocfinder/raw/master/index_files.ts --mock

Might also need '--unstable' flag.

To use the file index while searching for docs use '--index' arg:

    deno run --allow-env --allow-read --allow-write https://bitbucket.org/jackfiszr/cocfinder/raw/master/copy_cocs.ts --index --mock

Might also need '--unstable' flag.

# Merge the copied docs

Will create a joined docs pdf, but separate for each brand:

    deno run --allow-env --allow-read --allow-write https://bitbucket.org/jackfiszr/cocfinder/raw/master/merge_cocs.ts

Might also need '--unstable' flag.

# Watermark the copied docs

Will create another copy of the copied docs, but with a 'C O P Y' watermark:

    deno run --allow-env --allow-read --allow-write https://bitbucket.org/jackfiszr/cocfinder/raw/master/mark_cocs.ts

Might also need '--unstable' flag.

# Remove the copied docs

    deno run --allow-env --allow-read --allow-write https://bitbucket.org/jackfiszr/cocfinder/raw/master/clear_dir.ts

Might also need '--unstable' flag.

# Remove the split docs

    deno run --allow-env --allow-read --allow-write https://bitbucket.org/jackfiszr/cocfinder/raw/master/clear_split_cocs.ts --mock

Might also need '--unstable' flag.

## Advice

A convenient way of running the scripts is by executing the above commands from
shell/batch files.

Apart from not having to type each time the lengthy cmds you can also set there
some configuration like system path, http proxy etc.
