import { expandGlob, join, swissKnife } from "./deps.ts";
import { USER_DIR, USERNAME } from "./config.ts";

const globString = join(USER_DIR, "*.pdf");

for await (const pdf of expandGlob(globString)) {
  await Deno.remove(pdf.path);
  console.log(`rm ${USERNAME}/${pdf.name}`);
}

swissKnife.speak("Wszystko wytrute");
