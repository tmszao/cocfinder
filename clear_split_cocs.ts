import { existsSync, expandGlob, join } from "./deps.ts";
import { args, MAIN_INDEX_FILE, os, SEARCH_DIR, TEMP_DIR } from "./config.ts";

if (os !== "linux" && !args.mock) {
  throw ("This should be run with --mock flag");
}

const globString = join(SEARCH_DIR, "*", "*");

for await (const dir of expandGlob(globString)) {
  await Deno.remove(dir.path, { recursive: true });
  console.log(`rm ${dir.name}`);
}

if (existsSync(TEMP_DIR)) {
  await Deno.remove(TEMP_DIR, { recursive: true });
  console.log(`rm ${TEMP_DIR}`);
}

await Deno.create(MAIN_INDEX_FILE);
