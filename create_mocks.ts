import {
  ensureDir,
  existsSync,
  grayscale,
  join,
  PageSizes,
  PDFDocument,
  PDFPage,
} from "./deps.ts";
import {
  args,
  BRAND_DATA,
  MAIN_INDEX_FILE,
  os,
  SEARCH_DIR,
  SOURCE_DIR,
  VIN_LIST_FILE,
} from "./config.ts";

await main();

async function main() {
  if (os !== "linux" && !args.mock) {
    throw ("This should be run with --mock flag");
  }
  for (const br in BRAND_DATA) {
    if (BRAND_DATA[br]["PCL"]) {
      const path = join(SOURCE_DIR, BRAND_DATA[br]["PCL"]);
      await ensureDir(path);
    }
    if (BRAND_DATA[br]["PDF"]) {
      const path = join(SOURCE_DIR, BRAND_DATA[br]["PDF"]);
      await ensureDir(path);
      const brandWmis = BRAND_DATA[br]["WMI"];
      brandWmis.forEach((wmi: string) => {
        if (wmi === "WV2") {
          generateMockJointCocs(path, createMockVinSeries(wmi, 1, 2), true);
        } else {
          randomLengthTransfers(path, wmi, 1);
        }
      });
    }
    if (BRAND_DATA[br]["OUT"]) {
      const path = join(SEARCH_DIR, BRAND_DATA[br]["OUT"]);
      await ensureDir(path);
    }
  }
  if (!existsSync(MAIN_INDEX_FILE)) {
    await ensureDir(SEARCH_DIR);
    Deno.createSync(MAIN_INDEX_FILE);
  }
}

function randomLengthTransfers(
  path: string,
  firstThreeChars: string,
  maxFiles: number,
) {
  const numOfIteraions = Math.ceil(Math.random() * maxFiles);
  [...Array(numOfIteraions).keys()].forEach(async (_) => {
    const start = Math.floor(Math.random() * 10000);
    const seriesLength = Math.ceil(Math.random() * 100);
    await generateMockJointCocs(
      path,
      createMockVinSeries(firstThreeChars, start, seriesLength),
    );
  });
}

async function generateMockJointCocs(
  path: string,
  vinList: string[],
  duplicateVins?: boolean,
): Promise<void> {
  if (duplicateVins) {
    vinList = vinList.concat(vinList).sort();
  }
  const doubleVinList = vinList.concat(vinList).sort();
  const pdfDoc = await PDFDocument.create();
  const pages: PDFPage[] = [];
  const [pageHeight, pageWidth] = PageSizes.A4;
  const margin = 10;
  const fontSize = 30;
  const allCocs = vinList.length;
  const numOfPages = doubleVinList.length;

  const start = doubleVinList[0].substring(0, 3) + "_" +
    doubleVinList[0].substring(13);
  const end = doubleVinList[numOfPages - 1].substring(13);
  const tzoffset = (new Date()).getTimezoneOffset() * 60000;
  const datetime = (new Date(Date.now() - tzoffset)).toISOString().slice(0, -1)
    .replace(/:/g, "-");
  const filename = `${datetime}_JOINT.COCS.${start}-${end}_${allCocs}`; // creating pdf files without .pdf extension
  const filepath = join(path, filename);
  [...Array(numOfPages).keys()].forEach((_) =>
    pages.push(pdfDoc.addPage([pageWidth, pageHeight]))
  );
  pages.forEach((page, i) => {
    const currentVin = doubleVinList[i];
    const currentCoc = Math.ceil((i + 1) / 2);
    const pageNumber = i % 2 + 1;
    page.drawText(`CoC ${currentCoc} of ${allCocs} page ${pageNumber}`, {
      x: pageWidth / 2 + margin,
      y: pageHeight - fontSize - margin,
      size: fontSize,
      color: grayscale(0.5),
    });
    if (pageNumber === 1) {
      if (duplicateVins) {
        const stage = doubleVinList.indexOf(currentVin) === i ? 1 : 2;
        page.drawText(`stage ${stage}`, {
          x: margin * 2,
          y: pageHeight - fontSize - margin,
          size: fontSize,
          color: grayscale(0.5),
        });
      }
      page.drawText(`source: ${filepath}`, {
        x: 5,
        y: 5,
        size: fontSize / 2,
      });
    }
    page.drawText(currentVin, {
      x: pageWidth / 2 + margin,
      y: pageHeight / (4 * pageNumber),
      size: fontSize,
    });
  });
  const pdfBytes = await pdfDoc.save();
  await Deno.writeFile(filepath, pdfBytes);
}

function createMockVinSeries(
  firstThreeChars: string,
  start: number,
  seriesLength: number,
): string[] {
  const result = [];
  if (firstThreeChars.length !== 3) {
    throw ("The VIN beginning brand code should consist of exactly 3 characters");
  }
  if (start > 9999) {
    throw ("The start value should be a 4-digit number at most");
  }
  const maxVins = 10000 - start;
  if (maxVins < seriesLength) seriesLength = maxVins;
  console.log(`${seriesLength} VINs will be gereated`);
  for (let i = start; i < start + seriesLength; i++) {
    const vin = `${firstThreeChars}ZZZM0CKV1N${i.toString().padStart(4, "0")}`;
    result.push(vin);
  }
  console.log(result);
  addRandomToVinList(result);
  return result;
}

function addRandomToVinList(vinsArray: string[]) {
  const vin = vinsArray[Math.floor(Math.random() * vinsArray.length)];
  const encoder = new TextEncoder();
  const ecodedVin = encoder.encode(vin + "\n");
  Deno.writeFileSync(VIN_LIST_FILE, ecodedVin, { append: true });
}
