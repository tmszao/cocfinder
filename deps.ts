export const stdVer = "0.95.0";
export {
  copy,
  copySync,
  ensureDir,
  ensureDirSync,
  exists,
  existsSync,
  expandGlob,
  walk,
} from "https://deno.land/std@0.95.0/fs/mod.ts";
export {
  basename,
  dirname,
  join,
  sep,
} from "https://deno.land/std@0.95.0/path/mod.ts";
export { parse } from "https://deno.land/std@0.95.0/flags/mod.ts";
export * as fmt from "https://deno.land/std@0.95.0/fmt/colors.ts";
export {
  degrees,
  grayscale,
  PageSizes,
  PDFDocument,
  PDFPage,
  rgb,
} from "https://cdn.skypack.dev/pdf-lib@1.16.0?dts";
export * as cow from "https://deno.land/x/cowsay@1.1/mod.ts";
export { wait } from "https://deno.land/x/wait@0.1.10/mod.ts";
export * as swissKnife from "https://deno.land/x/swissKnife@1.1/mod.ts";
