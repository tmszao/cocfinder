import { copy, cow, fmt, join, sep, swissKnife, wait } from "./deps.ts";
import {
  BRAND_DIRS,
  MAIN_INDEX_FILE,
  SEARCH_DIR,
  USER_INDEX_FILE,
} from "./config.ts";
import { dlog, isEmptyDir, syncIndex } from "./utils.ts";

async function main() {
  const allDirs: string[] = [];
  const newDirs: string[] = [];
  const newFiles: string[] = [];

  dlog({
    color: "bgBlue",
    title: "INDEKSOWANIE",
    mainMsg: "Dodawanie nowych plików do listy",
    subMsg: new Date().toLocaleTimeString() + ", proszę czekać...\n",
  });

  await syncIndex();
  const spinner = wait({
    text: "wczytywanie indeksu plików...\n",
    spinner: "line",
  }).start();
  const INDEX_STR = await Deno.readTextFile(USER_INDEX_FILE);
  spinner.succeed();

  spinner.text = "listowanie katalogów...\n";
  spinner.start();
  for (const brandDir of BRAND_DIRS) {
    const brandDirPath = join(SEARCH_DIR, brandDir);
    for await (const el of Deno.readDir(brandDirPath)) {
      if (el.isDirectory) {
        allDirs.push([brandDir, el.name].join("/"));
      }
    }
  }
  spinner.succeed();

  spinner.text = "sprawdzanie nowych katalogów...\n";
  spinner.start();
  allDirs.forEach((dir, i) => {
    if (!INDEX_STR.includes(dir)) {
      newDirs.push(dir);
    }
    if (i % 10 === 0) {
      spinner.render();
    }
  });
  spinner.succeed();

  if (newDirs.length === 0) {
    console.log(fmt.bold(fmt.yellow("\nNA DYSKU NIE BYŁO NOWYCH FOLDERÓW")));
  } else {
    spinner.color = "yellow";
    spinner.text = "listowanie nowych plików...\n";
    spinner.start();
    for (const dir of newDirs) {
      const path = join(SEARCH_DIR, dir.split("/").join(sep));
      if (await isEmptyDir(path)) {
        spinner.stop();
        console.log(fmt.bold(fmt.red(`katalog ${path} jest pusty\n`)));
        spinner.start();
      }
      for await (const el of Deno.readDir(path)) {
        if (el.isFile) {
          newFiles.push(fmtPath(join(path, el.name)));
        }
      }
    }
    spinner.succeed();
  }

  if (newFiles.length > 0) {
    spinner.color = "magenta";
    spinner.text = "dodawanie do indeksu...\n";
    spinner.start();
    const result = newFiles.join("\n") + "\n";
    const encoder = new TextEncoder();
    const ecoded = encoder.encode(result);
    await Deno.writeFile(USER_INDEX_FILE, ecoded, { append: true });
    spinner.succeed();
    spinner.color = "cyan";
    spinner.text = "aktualizowanie indeksu głównego...\n";
    spinner.start();
    await copy(USER_INDEX_FILE, MAIN_INDEX_FILE, { overwrite: true });
    spinner.succeed();
    dlog({
      color: "bgGreen",
      title: "DOPISANO",
      mainMsg: "",
      subPrefix: "\n",
      subMsg: result,
    });
    swissKnife.speak("Dodałam wszyściutko");
    const msg = cow.say({
      text: "GREAT SUCCESS!",
      mode: Math.floor(Math.random() * 9),
      random: Math.floor(Math.random() * 10) === 9,
    });
    console.log(fmt.bold(fmt.green(msg)));
  } else {
    swissKnife.speak("Niec niewejszło");
    console.log(" > brak nowych plików");
  }
}

function fmtPath(path: string) {
  return path.replace(SEARCH_DIR + sep, "").split(sep).join("/");
}

main();
