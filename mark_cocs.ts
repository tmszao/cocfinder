import { degrees, PDFDocument, PDFPage, rgb, swissKnife } from "./deps.ts";
import { expandGlob, join } from "./deps.ts";
import { USER_DIR } from "./config.ts";

const globString = join(USER_DIR, "*.pdf");

for await (const pdf of expandGlob(globString)) {
  const text = "K O P I A";
  if (!pdf.name.includes(text)) {
    const pdfBytes = await markCoc(pdf.path, text);
    const filepath = pdf.path.replace(".pdf", `[${text}].pdf`);
    Deno.writeFileSync(filepath, pdfBytes);
    console.log(`Utworzono ${filepath}`);
  }
}
swissKnife.speak("Wszyściutko podstemplowałam");

async function markCoc(pdfPath: string, text: string) {
  const existingPdfBytes = Deno.readFileSync(pdfPath);
  const pdfDoc = await PDFDocument.load(existingPdfBytes);
  const helveticaFont = await pdfDoc.embedFont("Helvetica");
  const fontSize = 100;
  const pages = pdfDoc.getPages();
  pages.forEach((page: PDFPage) => {
    const rotationAngle = page.getRotation().angle;
    const { width, height } = page.getSize();
    const textWidth = helveticaFont.widthOfTextAtSize(text, fontSize);
    const diagonalTextDims = textWidth / Math.sqrt(2);
    const textShift = diagonalTextDims / 2;
    let x = width / 2 - textShift;
    let y = height / 2 + textShift;
    if (rotationAngle === 90) {
      y = height / 2 - textShift;
    }
    if (rotationAngle === 270) {
      x = width / 2 + textShift;
    }
    page.drawText(text, {
      x: x,
      y: y,
      size: fontSize,
      font: helveticaFont,
      color: rgb(0.95, 0.1, 0.1),
      rotate: degrees(-45 + rotationAngle),
      opacity: 0.5,
    });
  });
  return await pdfDoc.save();
}
