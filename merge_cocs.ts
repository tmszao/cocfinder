import { join, PDFDocument, PDFPage, swissKnife } from "./deps.ts";
import { USER_DIR } from "./config.ts";
import { brandNames, dlog, vinRegEx, wmiFor } from "./utils.ts";

main();

async function main() {
  for await (const brand of brandNames) {
    const cocsToMerge = getCocListForBrand(brand);
    if (cocsToMerge.length > 0) {
      const pdfBytes = await mergeCocs(cocsToMerge);
      const filename = `ALL_${brand}.pdf`;
      const filepath = join(USER_DIR, filename);
      Deno.writeFileSync(filepath, pdfBytes);
      dlog({
        color: "green",
        title: brand,
        mainMsg:
          `${cocsToMerge.length} CoC zostało połączonych w pliku ${filepath}\n`,
      });
    }
  }
  swissKnife.speak("Wszyściutko pozłączyłam");
}

async function mergeCocs(cocsToMerge: string[]) {
  const mergedPdf = await PDFDocument.create();
  for (const pdfPath of cocsToMerge) {
    const pdfBytes = Deno.readFileSync(pdfPath);
    const pdfDoc = await PDFDocument.load(pdfBytes);
    if (pdfDoc.getPageCount() === 2) {
      const copiedPages = await mergedPdf.copyPages(
        pdfDoc,
        pdfDoc.getPageIndices(),
      );
      copiedPages.forEach((page: PDFPage) => {
        mergedPdf.addPage(page);
      });
    }
  }
  const mergedPdfFile = await mergedPdf.save();
  return mergedPdfFile;
}

function getCocListForBrand(brand: string) {
  const result: string[] = [];
  const brandWmis = wmiFor[brand];
  for (const coc of Deno.readDirSync(USER_DIR)) {
    const vins = coc.name.match(vinRegEx);
    if (!vins) {
      continue;
    }
    const wmi = vins[0].substr(0, 3);
    if (brandWmis.includes(wmi)) {
      const cocPath = join(USER_DIR, coc.name);
      result.push(cocPath);
    }
  }
  return result;
}
