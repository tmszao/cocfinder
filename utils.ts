import {
  DISK_MAPPINGS,
  MAIN_INDEX_FILE,
  TEMP_DIR,
  USER_INDEX_FILE,
} from "./config.ts";
import { copy, ensureDir, exists, wait } from "./deps.ts";
export {
  dlog,
  pdfToTxt as cocToTxt,
  timeDiff,
  txtToCleanArr,
  verCheck,
} from "https://bitbucket.org/jackfiszr/utils/raw/v0.90.0/mod.ts";

export const wmiFor: {
  [brandName: string]: string[];
} = {
  AUDI: ["WAU", "WUA", "TRU"],
  PORSCHE: ["WP0"],
  SEAT: ["VSS"],
  SKODA: ["TMB"],
  VW: ["WVW", "WVG", "WV1", "WV2", "WV3"],
};

export const brandNames = Object.keys(wmiFor);

const allWmis = Object.values(wmiFor).flat();

export const vinRegEx = new RegExp(`(${allWmis.join("|")})\\w{14}`, "g"); // /(TMB|WVW|WVG|WV1|WV2|WV3|WAU|WUA|TRU|VSS)\w{14}/g

export function mapDisk(
  uncPath: string,
  diskLetter: keyof typeof DISK_MAPPINGS,
) {
  return uncPath.replace(DISK_MAPPINGS[diskLetter], `${diskLetter}:`);
}

export async function syncIndex() {
  if (!(await exists(USER_INDEX_FILE))) {
    await ensureDir(TEMP_DIR);
    const spinner = wait({
      text: "tworzenie lokalnej kopii indeksu...\n",
      spinner: "line",
    }).start();
    await copy(MAIN_INDEX_FILE, USER_INDEX_FILE);
    spinner.stop();
  } else {
    const mainIndexMtime = (await Deno.stat(MAIN_INDEX_FILE)).mtime;
    const userIndexMtime = (await Deno.stat(USER_INDEX_FILE)).mtime;
    if (
      mainIndexMtime && userIndexMtime &&
      mainIndexMtime > userIndexMtime
    ) {
      const spinner = wait({
        text: "aktualizowanie indeksu plików...\n",
        spinner: "line",
      }).start();
      await copy(MAIN_INDEX_FILE, USER_INDEX_FILE, { overwrite: true });
      spinner.stop();
    }
  }
}

export async function isEmptyDir(path: string) {
  for await (const el of Deno.readDir(path)) {
    return false;
  }
  return true;
}
